

# Locale Picker


LocalePicker is a fork of [country-picker-android](https://github.com/mukeshsolanki/country-picker-android) ([mukeshsolanki](https://github.com/mukeshsolanki)) which has been modified to be used with localization on Mastalab.

## How to use

### Integration


Step 1\. Add the JitPack repository to your build file. Add it in your root build.gradle at the end of repositories:

```java
allprojects {
  repositories {
    ...
    maven { url "https://jitpack.io" }
  }
}
```

Step 2\. Add the dependency - For the correct value of x.y, please refer to the [releases](https://github.com/stom79/country-picker-android/releases).

```java
dependencies {
        compile 'com.github.stom79:country-picker-android:x.y'
}
```

### Usage

Once the project has been added to gradle, the dialog can be easily used.

```java
CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
picker.setListener(new CountryPickerListener() {
    @Override
    public void onSelectCountry(String name, String locale, int flagDrawableResID) {
        // Implement your code here
    }
});
picker.show(getSupportFragmentManager(), "LOCALE_PICKER");
```

Missing locale? You can add it in the array in Country.java class

```java
new Country("Name for missing locale xx", "xx", R.drawable.flag_xx),
```