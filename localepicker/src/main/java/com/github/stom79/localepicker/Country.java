package com.github.stom79.localepicker;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;


import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by mukesh on 25/04/16.
 * Updated 01/11/17 by @stom79
 */

public class Country {
  private static final Country[] COUNTRIES = {
      new Country("Andorra", "ca", R.drawable.flag_ca),
      new Country("United Arab Emirates", "ar", R.drawable.flag_ar),
      new Country("Albania", "sq", R.drawable.flag_sq),
      new Country("Armenia", "hy", R.drawable.flag_hy),
      new Country("Azerbaijan",  "az", R.drawable.flag_az),
      new Country("Bosnia and Herzegovina", "bs",  R.drawable.flag_bs),
      new Country("Bangladesh",  "bn", R.drawable.flag_bn),
      new Country("Bulgaria",  "bg", R.drawable.flag_bg),
      new Country("China", "zh", R.drawable.flag_zh),
      new Country("Czech Republic", "cs", R.drawable.flag_cs),
      new Country("Germany", "de", R.drawable.flag_de),
      new Country("Denmark",  "da", R.drawable.flag_da),
      new Country("Spain", "es", R.drawable.flag_es),
      new Country("Ethiopia", "am", R.drawable.flag_am),
      new Country("Finland", "fi", R.drawable.flag_fi),
      new Country("France", "fr", R.drawable.flag_fr),
      new Country("United Kingdom", "en", R.drawable.flag_en),
      new Country("Georgia", "ka", R.drawable.flag_ka),
      new Country("Greece",  "el", R.drawable.flag_el),
      new Country("Croatia", "hr", R.drawable.flag_hr),
      new Country("Hungary", "hu", R.drawable.flag_hu),
      new Country("Indonesia",  "id", R.drawable.flag_id),
      new Country("Israel",  "he", R.drawable.flag_he),
      new Country("India", "hi", R.drawable.flag_in),
      new Country("Italy", "it", R.drawable.flag_it),
      new Country("Japan", "ja", R.drawable.flag_ja),
      new Country("South Korea", "ko", R.drawable.flag_ko),
      new Country("Lithuania", "lt", R.drawable.flag_lt),
      new Country("Latvia", "lv", R.drawable.flag_lv),
      new Country("Mongolia", "mn", R.drawable.flag_mn),
      new Country("Malaysia", "ms", R.drawable.flag_ms),
      new Country("Netherlands", "nl", R.drawable.flag_nl),
      new Country("Norway", "no", R.drawable.flag_no),
      new Country("Poland", "pl", R.drawable.flag_pl),
      new Country("Portugal", "pt", R.drawable.flag_pt),
      new Country("Romania", "ro", R.drawable.flag_ro),
      new Country("Serbia", "sr", R.drawable.flag_sr),
      new Country("Russia", "ru", R.drawable.flag_ru),
      new Country("Sweden", "sv", R.drawable.flag_sv),
      new Country("Slovakia",  "sk", R.drawable.flag_sk),
      new Country("Thailand", "th", R.drawable.flag_th),
      new Country("Turkey", "tr", R.drawable.flag_tr),
      new Country("Ukraine",  "uk", R.drawable.flag_uk),
      new Country("Viet Nam", "vi", R.drawable.flag_vi),
  };

  private String name;
  private String locale;
  private int flag = -1;

  private Country(String name, String locale, int flag) {
    this.name = name;
    this.locale = locale;
    this.flag = flag;
  }

  private Country() {
  }


  public String getName() {
    return name;
  }

  public int getFlag() {
    return flag;
  }

  public void setFlag(int flag) {
    this.flag = flag;
  }

  void loadFlagByCode(Context context) {
    if (this.flag != -1)
      return;

    try {
      this.flag = context.getResources()
          .getIdentifier("flag_" + this.locale.toLowerCase(Locale.ENGLISH), "drawable",
              context.getPackageName());
    } catch (Exception e) {
      e.printStackTrace();
      this.flag = -1;
    }
  }


    /*
     *      GENERIC STATIC FUNCTIONS
     */

  private static List<Country> allCountriesList;

  public static List<Country> getAllCountries() {
    if (allCountriesList == null) {
      allCountriesList = Arrays.asList(COUNTRIES);
    }
    return allCountriesList;
  }

  private static Country getCountryByISO(String countryIsoCode) {
    countryIsoCode = countryIsoCode.toUpperCase();

    Country c = new Country();
    c.setLocale(countryIsoCode);

    int i = Arrays.binarySearch(COUNTRIES, c, new ISOCodeComparator());

    if (i < 0) {
      return null;
    } else {
      return COUNTRIES[i];
    }
  }

  public static Country getCountryByName(String countryName) {
    // Because the data we have is sorted by ISO codes and not by names, we must check all
    // countries one by one

    for (Country c : COUNTRIES) {
      if (countryName.equals(c.getName())) {
        return c;
      }
    }
    return null;
  }

  public static Country getCountryByLocale(Locale locale) {
    String countryIsoCode = locale.getISO3Country().substring(0, 2).toLowerCase();
    return Country.getCountryByISO(countryIsoCode);
  }

  public static Country getCountryFromSIM(Context context) {
    TelephonyManager telephonyManager =
        (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    assert telephonyManager != null;
    if (telephonyManager.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
      return Country.getCountryByISO(telephonyManager.getSimCountryIso());
    }
    return null;
  }

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
    if (TextUtils.isEmpty(name)) {
      name = new Locale("", locale).getDisplayName();
    }
  }

    /*
     * COMPARATORS
     */

  public static class ISOCodeComparator implements Comparator<Country> {
    @Override
    public int compare(Country country, Country t1) {
      return country.locale.compareTo(t1.locale);
    }
  }


  public static class NameComparator implements Comparator<Country> {
    @Override
    public int compare(Country country, Country t1) {
      return country.name.compareTo(t1.name);
    }
  }
}